#!/usr/bin/env python3
import os
import subprocess
from pathlib import Path


def init_config_file_symlinks() -> None:
    """Ensures the config files can be found and then creates symlinks to them."""
    home: Path = Path(os.environ.get("HOME", ".")).absolute()
    assert home.exists()

    fah_vim_config_dir: Path = home / ".vim"
    assert fah_vim_config_dir.exists()

    vimrc: Path = fah_vim_config_dir / "vimrc"

    symlink_rc: Path = home / ".vimrc"
    neovim_config_dir: Path = home / ".config" / "nvim"
    symlink_neovim_config_dir: Path = (
        neovim_config_dir  # just make it clear we intend this to be a symlink
    )

    if symlink_rc.exists():
        target = symlink_rc.resolve().absolute()
        if not target == vimrc:
            print(
                "~/.vimrc exists and isn't part of the fah config! It points to ",
                target,
            )
    else:
        os.symlink(vimrc, symlink_rc)

    if symlink_neovim_config_dir.exists():
        neovim_config_dir.mkdir(parents=False, exist_ok=True)
        target = symlink_neovim_config_dir.resolve().absolute()
        if not target == fah_vim_config_dir:
            print(
                "~/.config/nvim exists and doesn't point at the fah config! It "
                f" points to {target}."
            )
    else:
        symlink_config_dir: Path = symlink_neovim_config_dir.parent
        symlink_config_dir.mkdir(parents=False, exist_ok=True)

        os.symlink(fah_vim_config_dir, symlink_neovim_config_dir)


def install_plugins() -> None:
    """Installs the vim and neovim plugins

    This should be idempotent and fully deletes and re-installs the pluginsgT."""
    subprocess.run(["python3", "install_plugins.py"], check=True)


def init_python_config() -> None:
    # [re]create the python env that we'll use for neovim
    subprocess.run(["bash", "init_py_neovim.sh"], check=True)


def main() -> None:
    """main entry point"""
    init_config_file_symlinks()
    install_plugins()
    init_python_config()


if __name__ == "__main__":
    main()
