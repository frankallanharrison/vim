" NOTE: Plugins are loaded via pathpack see `:help packages`
"       and the docs/help for them aren't setup by default. See `helptags All`
"       at the end of this vimrc.

" --------------
"  Editor config
"
filetype plugin indent on    " Enable filetype-specific indenting and plugins (:help :filetype)
syntax enable                " Syntax highlighting based on filetype (:help :syntax)

" Colour scheme
set termguicolors  " Enable true 24-bit color support in the terminal.
source $HOME/.vim/colors/carvedwoodcool.vim
colorscheme carvedwoodcool    " Colours set to the CarvedWoodCool theme

" Basic editor settings
set backspace=indent,eol,start  " allow backspace to work over elements named
set expandtab                   " Convert tabs to spaces (:help expandtab)
set foldlevelstart=99           " Set the fold-level on open to 99, meaning all folds are opened but we don't fuck with the other fold settings.
set hlsearch                    " Highlight search matches (:help hlsearch)
set hidden                      " Allow buffers to be put offscreen without saving first e.g. when using :bufdo
set ignorecase                  " Case-insensitive search (:help ignorecase)
set incsearch                   " Show search matches as you type (:help incsearch)
set nostartofline               " do not reset the cursor to the start of line when navigating
set nowrap                      " Disable line wrapping (:help wrap)
set number                      " Display line numbers (:help number)
set relativenumber              " Use relative numbers for easier navigation (:help relativenumber)
set ruler                       " Show cursor position in status line (:help ruler)
set scrolloff=5                 " Minimum lines around cursor when scrolling (:help scrolloff)
set shiftwidth=2                " Indentation level is 2 spaces (:help shiftwidth)
"set showmatch                  " When a bracket is inserted, briefly jump to the matching one.
set smartcase                   " Case-sensitive if uppercase used in search (:help smartcase)
set softtabstop=2               " Number of spaces that a <Tab> counts for while performing editing operations, like inserting a <Tab> or using <BS>.
set splitright                  " TODO: do I still want this: Open v-spit windows in right pane
set tabpagemax=25               " Set the maximum number of tabs concurrently opened (default is 10)
set tabstop=2                   " Tabs display as 2 spaces (:help tabstop)
set wildmenu                    " possible matches are shown just above the command line

" Disable the bell in vim
set noerrorbells
set novisualbell
set t_vb=
autocmd! GUIEnter * set vb t_vb=

" Indentation settings
set cindent                   " Enable C-style indentation (:help cindent)
set autoindent                " Copy indent from the previous line (:help autoindent)

" show tabs
set list                      " list-mode show the characters in `listchars`
set listchars=tab:→\          " when list-mode enabled, show tabs

" GUI-specific (i.e. not run in terminal) settings
if has("gui_running")
  set guifont=Andale\ Mono:h10   " GUI font
  set guioptions-=m           " Hide menu bar
  set guioptions-=T           " Hide toolbar
  set guioptions-=r           " Hide right-hand scroll bar
  set guioptions-=L           " Hide left-hand scroll bar
endif

" Autoread files if modified outside of editor
set autoread                  " Auto-reload files when changed externally (:help autoread)
if ! has("gui_running")
  autocmd FocusGained,BufEnter * checktime  " Auto-check if file modified when Vim is focused
endif

" ------------------
"  Commands
"
" Custom commands and mappings for quality of life
" Make 'W' act as 'write'
" - `-bang` allows ':W!'
" - `-bar` allows piping `:W | echo "Done"`
" - `-complete=file`: Enables file completion for command arguments, helping
"   with path suggestions.^
" - `-range=%`: Ensures the command can operate over a specified range,
"   defaulting to the entire file (%).
command! -bar -nargs=* -complete=file -range=% -bang W         <line1>,<line2>write<bang> <args>
" Make 'Write' act as 'write'
command! -bar -nargs=* -complete=file -range=% -bang Write     <line1>,<line2>write<bang> <args>
map :Wa :wa
map :Q :q
map :X :x

" ------------------
"  KEYBOARD MAPPINGS
"
" Set the <leader> key to <Space> (instead of '\')
let mapleader = " "

" Allow faster closing and browsing of the QuickFix list through `\qx` or
" `<space>qx`.
nnoremap <leader>qq :cclose<CR>
nnoremap <leader>qn :cnext<CR>
nnoremap <leader>qb :cprevious<CR>

" Load git interface
" NOTE: in nvim this is replaced by LazGit
nnoremap <leader>gg :Git<CR>

" Define mappings specifically for the Fugitive summary window
autocmd FileType fugitive nnoremap <buffer> <leader>g= =
autocmd FileType fugitive nnoremap <buffer> <leader>gd :Gdiffsplit<CR>
autocmd FileType fugitive nnoremap <buffer> <leader>gc :Gcommit<CR>
autocmd FileType fugitive nnoremap <buffer> <leader>gp :Gpush<CR>
autocmd FileType fugitive nnoremap <buffer> <leader>gs s
autocmd FileType fugitive nnoremap <buffer> <leader>gq :q<CR>

" Define mappings specifically for the Tig git browser
nnoremap <leader>gt :TigOpenProjectRootDir<CR>
nnoremap <Leader>gT :TigOpenCurrentFile<CR>
nnoremap <Leader>gb :TigBlame<CR>

" don't use builtin terminal for tig
let g:tig_explorer_use_builtin_term=0


" Makes CTRL-W CTL-V open a new buffer vertically, instead of current buffer again
nnoremap <c-w><c-v> :vnew<CR>
" Makes CTRL-W CTL-n open a new buffer vertically, instead of vertically
nnoremap <c-w><c-n> :vnew<CR>

" Disable arrow keys to enforce hjkl navigation
nnoremap <Left> :echo "No left for you! Use 'h'"<CR>
vnoremap <Left> :<C-u>echo "No left for you! Use 'h'"<CR>
inoremap <Left> <C-o>:echo "No left for you! Use 'h'"<CR>
nnoremap <Right> :echo "No Right for you! Use 'l'"<CR>
vnoremap <Right> :<C-u>echo "No Right for you! Use 'l'"<CR>
inoremap <Right> <C-o>:echo "No Right for you! Use 'l'"<CR>
nnoremap <Down> :echo "No Down for you! Use 'j'"<CR>
vnoremap <Down> :<C-u>echo "No Down for you! Use 'j'"<CR>
inoremap <Down> <C-o>:echo "No Down for you! Use 'j'"<CR>
nnoremap <Up> :echo "No Up for you! Use 'k'"<CR>
vnoremap <Up> :<C-u>echo "No Up for you! Use 'k'"<CR>
inoremap <Up> <C-o>:echo "No Up for you! Use 'k'"<CR>

" Move line up/down with Alt+j/k
" :m .-2 / :m .+1: Move the current line up or down relative to its position.
" ==: Auto-indents the moved line to match the context.
nnoremap <A-k> :m .-2<CR>==
nnoremap <A-j> :m .+1<CR>==

" mapping M-I should be alt-tab, but it doesn't seem to work. Mapping C-I (cntl-tab)
" seems to make both control and alt tab work??
map <M-`>      :call EAlt()<CR>

" Python configs
let g:ale_python_flake8_executable = 'python3'  " Linting tool with python3 (:help ale)

" ------------------
"  Other Config
"

if ! has('nvim')
  " Load matchit only in Vim (not Neovim)
  " `matchit` enhances % in Vim, allowing navigation between matching pairs
  " like HTML tags and code blocks.
  source $VIMRUNTIME/macros/matchit.vim
endif

" Show some nice hints for code-wdith.
if exists('+colorcolumn')
  " Check if the 'colorcolumn' option is supported by your version of Vim.
  " 'colorcolumn' is used to visually highlight specific columns in a text buffer.

  " Set the color column hints 80, and 88 columns for all files.
  " This is useful for adhering to best practises and readability across
  " systems.
  " - 80 is probably the most common limit for text width (e.g., for
  "   readability).
  " - 88 is (IIRC) the most efficient density vs width setting for code and is
  "   best for smallest file-length vs readbility as defined by python-black.
  set colorcolumn=80,88

  " For git-commit messages ADDITIONALLY set the color column hint at 55.
  " This is useful for adhering to commit message length conventions, where:
  " - 55 helps to fit commit messages in UI-tools like gitk and github.
  autocmd FileType gitcommit set colorcolumn=55,80,88
else
  " If 'colorcolumn' is not supported (older versions of Vim), use a
  " fallback. BUT just for the 88th character as that is 'best".

  " The following command highlights any characters that appear beyond the 88th column.
  " `matchadd()` is used to add a match group, which highlights text that meets a condition.
  " 'ErrorMsg' is used as the highlighting group, which typically renders the text in red.
  " `'\%>88v.\+'` is a regex pattern that matches any characters beyond the 88th column.
  " This acts as a visual indicator for text exceeding the preferred width.
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>88v.\+', -1)
endif

" Git commit
autocmd FileType gitcommit set spell " Spellcheck for git commits (:help spell)

" ALE linting settings
"   - inc. ALE linting settings to control when linting occurs
"   - To see ALE issues in the quickfix window do `:copen`
let g:ale_set_quickfix = 1             " Show ALE issues in quickfix window (:help ale)
let g:ale_list_window_size = 5         " Quickfix window size for ALE
let g:ale_lint_on_text_changed = 0     " Disable linting as text changes, avoids constant linting feedback
let g:ale_lint_on_enter = 0            " Disable linting on buffer enter, so files don't auto-lint when opened
let g:ale_lint_on_save = 0             " Disable linting on save to focus on edits, not interim checks
let g:ale_lint_on_filetype_changed = 0 " Disable linting when file type changes, keeps focus on actual edits
let g:ale_lint_on_insert_leave = 1     " Enable linting when exiting insert mode, allows a quick check after edits

" Airline status bar settings
let g:airline#extensions#tabline#enabled = 1

" Syntax mappings:
" `vimoutliner` files
au! BufRead,BufNewFile *.otl            setfiletype vo_base
" Actionscript files
au! BufNewFile,BufRead *.as setlocal ft=javascript
" shader files
au! BufNewFile,BufRead *.shader setlocal ft=c
" SConscripts with .py extensions
au! BufNewFile,BufRead SConscript setlocal ft=python
" QMake files with .ppro extensions
"au! BufNewFile,BufRead *.pro setlocal ft=cmake
"
" Automatically generate helptags for plugins in pack directories
autocmd VimEnter * if empty(glob("~/.vim/pack/*/start/*/doc/tags")) | silent! helptags ALL | endif
