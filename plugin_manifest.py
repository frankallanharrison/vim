#!/usr/bin/env python3
"""
Defines the set of plugins I use in vim and neovim.
"""

from pathlib import Path
from typing import List

from utils.print_plugin import print_plugin_list
from utils.synchronous_sub_proc_run import synchronous_sub_proc_run
from utils.types import PluginDef

# the new path is packpath, we'll use the `default` arena for now.
PACK_PATH: Path = Path("pack/default/start")


def lsp_post_install_callback(install_path: Path) -> None:
    """Install all the language servers we use

    NOTE: osx specific and required homebrew
    """
    python_install = ["npm", "install", "-g", "pyright"]
    tsserver_install = [
        "npm",
        "install",
        "-g",
        "typescript",
        "typescript-language-server",
    ]
    # clangd_install = ["brew", "install", "llvm"]
    markdown_marksman_install = ["brew", "install", "marksman"]
    lua = ["brew", "install", "lua-language-server"]
    print("Installing LSP language servers")
    for cmd in (
        python_install,
        tsserver_install,
        # TODO: when I start using cpp again: clangd_install,
        markdown_marksman_install,
        lua,
    ):
        print(cmd)
        synchronous_sub_proc_run(cmd)


PLUGIN_MODULES: List[PluginDef] = [
    (
        "github.com/ThePrimeagen/refactoring.nvim.git",
        "refactoring.nvim",
        "refactoring by Martin Fowler, implemented by ThePrimeagen",
        ":Refactor extract new_func_name",
    ),
    (
        "github.com:hrsh7th/nvim-cmp.git",
        "nvim-cmp.nvim",
        "Code-complete for nvim",
        "TODO",
    ),
    (
        "github.com:hrsh7th/cmp-nvim-lsp.git",
        "cmp-nvim-lsp.nvim",
        "An LSP-source using nvim-cmp",
        "TODO",
    ),
    (
        "github.com/nvim-tree/nvim-web-devicons.git",
        "nvim-web-devicons",
        "dev-icons for Which Key",
        "for mini-icons",
    ),
    (
        "github.com/folke/which-key.nvim.git",
        "which-key.nvim",
        "Which Key for [re]discovering shortcuts",
        "<leader> ... pause, shows all leader keys",
    ),
    (
        "github.com/nvim-treesitter/nvim-treesitter.git",
        "nvim-treesitter.git",
        "Treesitter syntax-highlighting",
        (
            "enables high quality and fast syntax highlighting. "
            "e.g. gd gD gi gr aka go to def/dec/imp/refs :help ale_python"
            "e.g. TODO: code-complete"
        ),
        lsp_post_install_callback,
    ),
    (
        "github.com/neovim/nvim-lspconfig.git",
        "lsp.git",
        "Language server config",
        (
            "enables code-browsing and auto-complete "
            "e.g. gd gD gi gr aka go to def/dec/imp/refs :help ale_python"
            "e.g. TODO: code-complete"
        ),
        lsp_post_install_callback,
    ),
    (
        "github.com/dense-analysis/ale.git",
        "ale.git",
        "Asynchronous Lint Engine",
        ":help ale_python",
    ),
    (
        "github.com/nvim-lua/plenary.nvim.git",
        "nvim-lua-plenary.nvim",
        "Needed by telescope",
        r"no idea",
    ),
    (
        "github.com/nvim-telescope/telescope.nvim.git",
        "telescope.nvim.git",
        "Super-fast fuzzy opening of files, buffers, commands, etc.",
        r"<Leader>t, <Leader>g, `:echo mapleader` is backslash (\) by default",
    ),
    (
        ("github.com/ThePrimeagen/harpoon.git", "harpoon2"),
        "harpoon2.nvim.git",
        "Super-fast switching between files (requires plenary)",
        r"add buffer `<leader>a`, list: <C-f>, b1: <C-h>, b2: <C-j>, b3: <C-k>, b4:<C-l>",
    ),
    (
        "github.com/tpope/vim-surround.git",
        "surround.git",
        "Change surrounding chars",
        "cs'<p>, :help surround",
    ),
    (
        "github.com/tpope/vim-fugitive",
        "fuGITive.git",
        "Git wrapper in vim (overridden with lazy git in nvim)",
        ":help fugitive, :help :Git, `<leader>gb` (blame), '<leader>gs` (status)",
    ),
    (
        "github.com/kdheepak/lazygit.nvim.git",
        "lazygit.nvim.git",
        "The awesome lazy-git inside nvim",
        ":help LazyGit",
    ),
    (
        "github.com/reinh/vim-makegreen.git",
        "makegreen.git",
        "Show test-run status",
        ":MakeGreen %",
    ),
    (
        "github.com/plasticboy/vim-markdown.git",
        "vim-markdown.git",
        "Highlighting for .md",
        ":h vim-markdown",
    ),
    (
        "github.com/mhinz/vim-signify.git",
        "vim-signify.git",
        "Shows diff in the gutter",
        ":h signify",
    ),
    (
        "github.com/tpope/vim-abolish.git",
        "abolish.git",
        "Subvert, coercion (crs, cru), Abbreviation",
        "h: crs, h:abbrviations, :Subvert, really nice",
    ),
    (
        "github.com/vimoutliner/vimoutliner.git",
        "vimoutliner.git",
        "Quick note taking and outlineing of tasks/information",
        "h: vo, use ',,', <leader>cb, <leader>cp, <leader>s, <leader>S",
    ),
    (
        "github.com:vim-airline/vim-airline.git",
        "vim-airline.git",
        "status-bar",
        "There's lots of features, see https://github.com/vim-airline/vim-airline",
    ),
    (
        "github.com:iberianpig/tig-explorer.vim",
        "tig-explorer.vim",
        "Allows tig in vim. I use tig as a console-base gitk, but it has much more.",
        ":Tig",
    ),
]

PYTHON_PLUGINS = [
    (
        "github.com/alfredodeza/pytest.vim.git",
        "py.test.git",
        "Py.test wrapper",
        ":h test",
    ),
]
PLUGIN_MODULES.extend(PYTHON_PLUGINS)

# # Add snipmate support
# PLUGIN_MODULES.extend(
#     (
#         (
#             "github.com/garbas/vim-snipmate.git",
#             "snipmate.git",
#             "Extra snippet on <tab>",
#             ":help snipMate",
#         ),
#         (
#             "github.com/MarcWeber/vim-addon-mw-utils.git",
#             "vim-addon-mw-utils",
#             "Supports snipmate",
#             ":help snipMate",
#         ),
#         (
#             "github.com/tomtom/tlib_vim.git",
#             "tlib_vim",
#             "Supports snipmate",
#             ":help snipMate",
#         ),
#     )
# )


def main() -> None:
    """main entry point"""
    print_plugin_list(PLUGIN_MODULES, pack_path=PACK_PATH)


if __name__ == "__main__":
    main()
