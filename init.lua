-- Source the vimrc file that contains common vim and neovim settings and key
-- bindings
-- Was:
-- 	source ~/.vimrc
vim.cmd("source ~/.vimrc")

-- Source the local configs
vim.cmd("source ~/.vim/neovim.local.py.vim")

-- Disable Node.js remote plugins
-- I don't think I can about remote plugins via Nodejs
vim.g.loaded_node_provider = 0

-- Enable telescope for faster fuzzy-finding
require("telescope").setup({
  defaults = {
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      -- Allow grepping through hidden
      --    The intent here is for this to grep all normal files and `.file` files.
      "--hidden", -- Include hidden files (`.filename` and hide-flagged files)
      "--glob",
      "!.git/", -- Exclude .git directory
    },
  },
})

-- Load the config for lsp and other code tools
require("cfg_lsp")

-- setup harpoon early (seems to crash without it!)
local harpoon = require("harpoon")
-- REQUIRED
harpoon:setup()
