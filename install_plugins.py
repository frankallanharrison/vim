#!/usr/bin/env python3
import argparse
import datetime
import os
import platform
import shutil
import subprocess
import sys
from pathlib import Path
from typing import List, Literal, Optional

from plugin_manifest import PACK_PATH, PLUGIN_MODULES
from utils.decompose_info import decompose
from utils.print_plugin import print_plugin_list
from utils.synchronous_sub_proc_run import (
    NonZeroExitCodeError,
    synchronous_sub_proc_run,
)
from utils.types import GitBranchName, GitUri


class DirtyGitDir(RuntimeError):
    """Thrown when we detect changes in a git-checkout."""


def check_git_clean() -> None:
    """Check if there are uncommitted changes in the current Git repository.

    Raises:
        RuntimeError: If there are any uncommitted or unstaged changes.
    """
    try:
        # Check for uncommitted changes
        result = subprocess.run(
            ["git", "status", "--porcelain"], capture_output=True, text=True, check=True
        )

        # If output is not empty, there are changes
        if result.stdout.strip():
            raise DirtyGitDir(
                "Uncommitted changes detected. Please commit or stash your changes."
            )
        else:
            print("No changes detected. Repository is clean.")

    except subprocess.CalledProcessError as e:
        raise DirtyGitDir("Failed to check git status") from e


def _detect_os() -> Literal["Linux", "macOS", "Other"]:
    """
    Detect the operating system.

    Returns:
        Literal["Linux", "macOS", "Other"]: The detected operating system.
    """
    system_name = platform.system()
    if system_name == "Linux":
        return "Linux"
    elif system_name == "Darwin":  # macOS identifies as 'Darwin'
        return "macOS"
    else:
        return "Other"


def backup_plugins_dir(bundle_path: Path) -> None:
    time_stamp = str(datetime.datetime.now(datetime.UTC))
    time_stamp = time_stamp.translate(str.maketrans(" -:.", "_" * 4))
    try:
        shutil.move(bundle_path, "%s-%s-bak" % (bundle_path, time_stamp))
    except FileNotFoundError:
        pass


def reset_plugins_dir_submodules() -> None:
    """hard resets the git state of the plugin submodules.

    This means we can ensure the clean-state of the config before continuing."""
    # reset all submodule before continuing
    for cmd in (
        ["git", "submodule", "update", "--init", "--recursive"],
        ["git", "submodule", "foreach", "--recursive", "git", "reset", "--hard"],
        ["git", "submodule", "foreach", "git", "clean", "-fdx"],
    ):
        try:
            synchronous_sub_proc_run(cmd)
        except NonZeroExitCodeError:
            print(f"WARNING: failed to reset submodules: '{' '.join(cmd)}'")


def get_correct_prefix(MODULE_URL: str, use_ssh: bool) -> str:
    if use_ssh:
        prefix = "git@"
        MODULE_URL = MODULE_URL.replace("github.com/", "github.com:")
    else:
        prefix = "https://"
    print(MODULE_URL)
    MODULE_URL = prefix + MODULE_URL
    return MODULE_URL


def installModule(
    MODULE_URL: GitUri,
    branch_name: GitBranchName,
    INSTALL_LOCATION: Path,
    use_ssh: bool,
) -> None:
    MODULE_URL = get_correct_prefix(MODULE_URL, use_ssh)

    # delete any existing submodule as the path may have changed.
    try:
        cmd: List[str] = ["git", "rm", "-f", "--cached", str(INSTALL_LOCATION)]
        synchronous_sub_proc_run(cmd)
    except NonZeroExitCodeError:
        # print(f"WARNING: failed to delete plugin cache for {INSTALL_LOCATION}")
        pass

    try:
        cmd = ["git", "submodule", "sync", "--recursive"]
        synchronous_sub_proc_run(cmd)
    except NonZeroExitCodeError:
        print(
            f"WARNING: failed to sync git submodules after clearing{INSTALL_LOCATION}"
        )

    branch_flags: List[str] = []
    if branch_name is not None:
        branch_flags = ["-b", branch_name]

    cmd = [
        "git",
        "submodule",
        "add",
        *branch_flags,
        "--force",
        MODULE_URL,
        str(INSTALL_LOCATION),
    ]
    synchronous_sub_proc_run(cmd)


def install_fonts() -> None:
    font_install_dir: Optional[Path] = None
    font_source_dir: Path = Path("%s/.vim/fonts" % os.environ.get("HOME", ".."))
    if _detect_os() == "Linux":
        # Make the install dir for fonts
        font_install_dir = Path("%s/.fonts" % os.environ.get("HOME", "..")).absolute()
        os.makedirs(font_install_dir, exist_ok=True)
    if _detect_os() == "macOS":
        font_install_dir = Path(
            "%s/Library/Fonts" % os.environ.get("HOME", "..")
        ).absolute()
    assert font_install_dir is not None

    font_files = list(font_source_dir.rglob("*.ttf")) + list(
        font_source_dir.rglob("*.otf")
    )
    for font_file in font_files:
        print(font_file)
        shutil.copy(font_file, font_install_dir)
    if _detect_os() == "Linux":
        # on linux using fc-cache to install the fonts
        cmd = ["fc-cache", "-f", str(font_install_dir)]
        synchronous_sub_proc_run(cmd)


def reset_all_submodules() -> None:
    """a function whose intent is to clear all submodule state.

    This is so we can reinstall it fully and have a pristine.

    This core idea is that the reinstall of submodules is idempotent and
    therefore the .gitimodules etc. shouldn't change unless there *ARE VALID
    CHANGES*
    """
    cmd = ["git", "submodule", "deinit", "-f", "--all"]
    try:
        synchronous_sub_proc_run(cmd)
    except NonZeroExitCodeError:
        pass

    # Clear the .gitmodules file as deleting it just seems to break submodules,
    # probably because they've beein initialised and a .git/config file
    # references them.
    gitmodules_file = Path(".gitmodules")
    gitmodules_file.write_text("")


def _parse_args(argv: List[str]) -> argparse.Namespace:
    """
    Parse command-line arguments.

    Args:
        argv (List[str]): List of arguments passed to the program.

    Returns:
        Namespace: Parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Install vim and neovim plugin.")
    parser.add_argument(
        "--allow-dirty-git",
        action="store_true",
        default=False,
        help="Allow operations on a dirty git repository. Default is False.",
    )
    return parser.parse_args(argv[1:])


def main() -> None:  # noqa C901
    args = _parse_args(sys.argv)

    bundle_path_deprecated: Path = Path(
        "%s/.vim/bundle" % os.environ.get("HOME", ".")
    ).absolute()
    if bundle_path_deprecated.exists():
        # we're migrating to the new format from old.
        backup_plugins_dir(bundle_path_deprecated)

    cmd: List[str] = []

    # after backing up and before checking for clean-checkout state, reset the
    # submodule back to prestine, assuming that the install-steps will put things
    # back into their correct state. Note that the backup above will protect any
    # anoying missed changes.
    reset_plugins_dir_submodules()

    # before running check that we have a clean directory.
    if not args.allow_dirty_git:
        check_git_clean()

    # Cleanup and delete existing submodules before re-adding valid ones.
    for plugin_path in ("bundle/", str(PACK_PATH)):
        try:
            synchronous_sub_proc_run(["git", "rm", "--cached", "-rf", plugin_path])
        except NonZeroExitCodeError:
            print("WARNING: failed to delete, from git, the bundle/ dir")
            pass  # do not re-raise
        synchronous_sub_proc_run(["rm", "-rf", plugin_path])

    synchronous_sub_proc_run(["rm", "-rf", "bundle/"])

    # Make the install dir for plugins, noting that it should have been cleaned up
    # by now.
    PACK_PATH.parent.parent.mkdir(exist_ok=True, parents=False)
    PACK_PATH.parent.mkdir(exist_ok=True, parents=False)
    PACK_PATH.mkdir(exist_ok=True, parents=False)

    USE_SSH = True

    reset_all_submodules()

    synchronous_sub_proc_run(["git", "submodule", "update", "--remote", "--merge"])

    for PLUGIN_INFO in PLUGIN_MODULES:
        (
            url,
            branch_name,
            pretty_name,
            summary,
            install_location,
            my_mini_doc,
            post_install_callback,
        ) = decompose(
            PLUGIN_INFO,
            pack_path=PACK_PATH,
        )
        print(f"installing: {pretty_name}")

        assert post_install_callback is None or callable(
            post_install_callback
        ), post_install_callback

        installModule(url, branch_name, install_location, USE_SSH)

        if post_install_callback:
            post_install_callback(install_location)

        print(f"running `git add {install_location}")
        cmd = ["git", "add", str(install_location)]
        synchronous_sub_proc_run(cmd)

    post_setup_init_cmds = (
        ["git", "submodule", "init"],
        ["git", "submodule", "update", "--init", "--recursive"],
        ["git", "submodule", "foreach", "git", "submodule", "init"],
        ["git", "submodule", "foreach", "git", "submodule", "update"],
    )
    for cmd in post_setup_init_cmds:
        synchronous_sub_proc_run(cmd)
    try:
        install_fonts()
    except Exception as err:  # noqa
        print(f"ERROR: fonts failed to install! {err}")
        print("if font installs fail on OSX `brew install fontconfig` should fix it")
        sys.exit(1)

    # Print the hicely formated help
    print_plugin_list(PLUGIN_MODULES, pack_path=PACK_PATH)

    # after running also check that we have a clean directory and install didn't
    # cause any side effects.
    try:
        check_git_clean()
        print("DONE")
    except DirtyGitDir:
        print("WARNING: git status reports changes after install")


if __name__ == "__main__":
    main()
