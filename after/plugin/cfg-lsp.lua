-- code actions
vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { desc = "Code Action", silent = true, noremap = true })
vim.keymap.set({ "n", "x" }, "<leader>cn", ":cnext<CR>", { desc = "Next error" })
vim.keymap.set({ "n", "x" }, "<leader>cb", ":cprevious<CR>", { desc = "Prev error" })

-- ALE actions
vim.keymap.set("n", "<leader>cd", ":ALEDetail<CR>", { desc = "ALE details", silent = true, noremap = true })
vim.keymap.set({ "n", "x" }, "<leader>cj", ":ALENext<CR>", { desc = "Next ALE error" })
vim.keymap.set({ "n", "x" }, "<leader>ck", ":ALEPrevious<CR>", { desc = "Prev ALE error" })

-- code editing shortcuts
--  - Declaration: Introduces an entity and its type.
--    It tells the compiler "this thing exists and here’s what it looks like."
--  - Definition: Provides the full implementation or the actual details of the
--    entity.
--    It tells the compiler or runtime "this is exactly how it behaves or what
--    it is."
--  We use telescope where we can
local telescope_builtin = require("telescope.builtin")
vim.keymap.set("n", "gd", telescope_builtin.lsp_definitions, { desc = "Go to Definition (Telescope)" })
vim.keymap.set("n", "gi", telescope_builtin.lsp_implementations, { desc = "Go to Implementation (Telescope)" })
vim.keymap.set("n", "gr", telescope_builtin.lsp_references, { desc = "Find References (Telescope)" })
-- No "declaration" support in telescope.
vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { desc = "Go to Declaration (lsp)" })
-- Alternatives to using telescope, using lsp directly:
-- vim.keymap.set("n", "gd", vim.lsp.buf.definition, { desc = "Go to Definition" })
-- vim.keymap.set("n", "gi", vim.lsp.buf.implementation, { desc = "Go to Implementation" })
-- vim.keymap.set("n", "gr", vim.lsp.buf.references, { desc = "Find References" })
vim.keymap.set("n", "<leader>ws", vim.lsp.buf.workspace_symbol, { desc = "Workspace Symbols (lsp)" })

-- refactoring combos
-- `<space>rf`.
-- NOTE: {"n", "x"} is normal and visual-select mode
vim.keymap.set({ "n", "x" }, "<leader>rf", ":Refactor extract", { desc = "Extract function" })
vim.keymap.set({ "n", "x" }, "<leader>re", ":Refactor extract", { desc = "Extract" })
vim.keymap.set({ "n", "x" }, "<leader>rv", ":Refactor extract_var", { desc = "Extract variable" })
vim.keymap.set({ "n", "x" }, "<leader>rn", vim.lsp.buf.rename, { desc = "Rename symbol" })
vim.keymap.set({ "n", "x" }, "<leader>rr", function()
  require("telescope").extensions.refactoring.refactors()
end)

-- Setup refactoring tools
-- refactoring.nvim done after treesitter
local refactoring = require("refactoring")
refactoring.setup({
  -- prompt_func_return_type = {
  --   go = false,
  --   java = false,

  --   cpp = false,
  --   c = false,
  --   h = false,
  --   hpp = false,
  --   cxx = false,
  -- },
  -- prompt_func_param_type = {
  --   go = false,
  --   java = false,

  --   cpp = false,
  --   c = false,
  --   h = false,
  --   hpp = false,
  --   cxx = false,
  -- },
  -- printf_statements = {},
  -- print_var_statements = {},
  -- show_success_message = true, -- shows a message with information about the refactor on success
  -- -- i.e. [Refactor] Inlined 3 variable occurrences
})

-- Load the "refactoring" extension for the telescope plugin to enable advanced
-- code refactoring features
require("telescope").load_extension("refactoring")
