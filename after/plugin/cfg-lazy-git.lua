-- LazyGit shortcuts
vim.keymap.set({ "n", "x" }, "<leader>gg", ":LazyGit<CR>", { desc = "Load LazyGit" })
vim.keymap.set({ "n", "x" }, "<leader>gb", ":Git blame", { desc = "blame" })
vim.keymap.set({ "n", "x" }, "<leader>gs", ":Git status", { desc = "status (lazygit is better)" })
