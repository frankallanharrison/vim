-- Markdown config
-- Set the conceal level, mainly for markdown, but might effect other languages
-- too
-- Enable conceal in markdown files
vim.api.nvim_create_autocmd("FileType", {
  pattern = "markdown",
  callback = function()
    vim.opt_local.conceallevel = 2
    vim.opt_local.concealcursor = "n"
  end,
})
