" Vim color file
" carvedwood v0.1
" Maintainer:        Shawn Axsom <axs221@gmail.com>
"               [axs221.1l.com]

" carvedwood -
"     a cool blue version of my color scheme modified from my desertocean scheme into a brown
" and green scheme, easier on the eyes and optimized for more important syntax
" to stand out the most (eg comments and values are dark and dull while
" statements are bright).

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
                syntax reset
    endif
endif

let g:colors_name="carvedwoodcool"

hi Comment                                ctermfg=darkcyan                 guifg=#6d303d
hi Constant                               ctermfg=brown                    guifg=#55464a
hi ColorColumn             ctermbg=4                         guibg=#104550 guifg=#6aa09a
hi Cursor                                                    guibg=#104550 guifg=#6aa09a
hi DiffAdd                 ctermbg=4
hi diffAdded                              ctermfg=green
hi DiffChange              ctermbg=5
hi DiffDelete   cterm=bold ctermbg=6      ctermfg=4
hi diffRemoved                            ctermfg=red
hi DiffText     cterm=bold ctermbg=1
hi Directory                              ctermfg=darkcyan                 guifg=#bbd0df
hi Error        cterm=bold ctermbg=1      ctermfg=7          guibg=#e04462
hi ErrorMsg     cterm=bold ctermbg=1      ctermfg=7
hi FoldColumn              ctermbg=NONE   ctermfg=7          guibg=#3a4a5a guifg=#00CCFF
hi Folded                  ctermbg=NONE   ctermfg=7          guibg=#4a4f4d guifg=#BBDDCC
hi Identifier                             ctermfg=6                        guifg=#d07a70
hi Ignore       cterm=bold                ctermfg=7                        guifg=grey40
hi IncSearch    cterm=NONE ctermbg=112    ctermfg=235        guibg=#cddaf0 guifg=#50606d
hi LineNr                                 ctermfg=3          guibg=#153040 guifg=#D0C5c0
hi ModeMsg      cterm=NONE                ctermfg=brown                    guifg=#00AACC
hi MoreMsg                                ctermfg=darkgreen                guifg=SeaGreen
hi NonText      cterm=bold                ctermfg=darkblue   guibg=#1c3447 guifg=#382920
hi Normal                                                    guibg=#05181c guifg=#aa9aa3
hi PmenuSel                ctermbg=NONE   ctermfg=166        guibg=#3a4a5a guifg=#00CCFF
hi PmenuSbar               ctermbg=NONE   ctermfg=166        guibg=#3a4a5a guifg=#00CCFF
hi PmenuThumb              ctermbg=NONE   ctermfg=166        guibg=#3a4a5a guifg=#00CCFF
hi Pmenu                   ctermbg=16     ctermfg=9          guibg=#4a4f4d guifg=#BBDDCC
hi PreProc                                ctermfg=5                        guifg=#aa7065 gui=none
hi Question                               ctermfg=green                    guifg=#AABBCC
hi Search       cterm=NONE ctermbg=173    ctermfg=230        guibg=#5a6d7d guifg=#bac5d0
hi Special                                ctermfg=5                        guifg=#556065
hi SpecialKey                             ctermfg=darkgreen                guifg=#90703B
hi Statement                              ctermfg=3                        guifg=#f0caba
hi StatusLine   cterm=bold,reverse                           guibg=#d0a59a guifg=#102015 gui=none
hi StatusLineNC cterm=reverse                                guibg=#937b7a guifg=#373334 gui=none
hi Title                                  ctermfg=5                        guifg=#60b0ea
hi Todo                                                      guibg=yellow  guifg=orangered
hi Type                                   ctermfg=2                        guifg=#c09a8a
hi Underlined   cterm=underline           ctermfg=5                        guifg=#80aae0
hi VertSplit    cterm=reverse                                guibg=#c2bfa5 guifg=grey50 gui=none
hi Visual       cterm=NONE ctermbg=172    ctermfg=230        guibg=#33DFEF guifg=#008FBF
hi Visual       cterm=NONE ctermbg=172    ctermfg=161        guibg=#33DFEF guifg=#008FBF
hi WarningMsg                             ctermfg=1                        guifg=salmon
hi WildMenu                ctermbg=3      ctermfg=0

"vim: sw=4
