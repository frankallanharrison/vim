-- Install the lsp settings so we can do thing like go-to-symbol
-- NOTE: as this is the first require, that the quotes must be double " instead
-- of ', sigh
local lspconfig = require("lspconfig")

-- Add nvim-cmp for autocompletion
local cmp = require("cmp")
-- TODO: local lspkind = require("lspkind") -- Optional for fancy icons

cmp.setup({
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body) -- Use LuaSnip or vsnip
    end,
  },
  mapping = {
    ["<Tab>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
    ["<S-Tab>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
    ["<CR>"] = cmp.mapping.confirm({ select = true }),
    ["<C-Space>"] = cmp.mapping.complete(),
  },
  sources = {
    { name = "nvim_lsp" },
    { name = "buffer" },
    { name = "path" },
  },
  -- formatting = {
  --   format = lspkind.cmp_format({
  --     mode = "symbol", -- Show only symbol icons
  --     maxwidth = 50,
  --   }),
  -- },
})

-- Attach LSP capabilities to `nvim-cmp`
-- We pass these to the servers
local capabilities = require("cmp_nvim_lsp").default_capabilities()

-- Python
lspconfig.pyright.setup({ capabilities = capabilities })

-- JavaScript/TypeScript
lspconfig.ts_ls.setup({ capabilities = capabilities })

-- C++
lspconfig.clangd.setup({ capabilities = capabilities })

-- Markdown
lspconfig.marksman.setup({ capabilities = capabilities })

-- Set up the Lua language server
lspconfig.lua_ls.setup({
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the server which version of Lua you’re using
        version = "LuaJIT", -- For Neovim
        path = vim.split(package.path, ";"),
      },
      diagnostics = {
        -- Enable diagnostics for global variables like `vim`
        globals = { "vim" },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = {
          [vim.fn.expand("$VIMRUNTIME/lua")] = true,
          [vim.fn.stdpath("config") .. "/lua"] = true,
        },
        checkThirdParty = false, -- Avoid prompts for third-party tools
      },
      telemetry = {
        enable = false, -- Disable telemetry
      },
    },
  },
})

require("nvim-treesitter.configs").setup({
  -- A list of parser names, or "all" (the listed parsers MUST always be installed)
  ensure_installed = {
    "c",
    "javascript",
    "lua",
    "markdown",
    "markdown_inline",
    "python",
    "query",
    "tsx",
    "toml",
    "typescript",
    "vim",
    "vimdoc",
  },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (or "all")
  ignore_install = {},

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers",
  -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    -- disable = { "c", "rust" },
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
    -- disable = function(lang, buf)
    --   local max_filesize = 100 * 1024 -- 100 KB
    --   local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    --   if ok and stats and stats.size > max_filesize then
    --     return true
    --   end
    -- end,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
})

vim.cmd([[
  autocmd BufRead,BufNewFile *.rc set filetype=toml
]])
