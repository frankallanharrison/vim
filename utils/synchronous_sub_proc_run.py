"""A utility for running command lines synchronously"""

import subprocess
from pathlib import Path
from typing import List, Optional


class NonZeroExitCodeError(RuntimeError):
    """Thrown when we detect an error in synchronous_sub_proc_run()"""


def synchronous_sub_proc_run(
    cmd_list: List[str], error_msg: str = "", cwd: Optional[Path] = None
) -> None:
    proc_run_info = subprocess.run(
        cmd_list, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    ret_code = proc_run_info.returncode
    if ret_code != 0:
        if b"already exists in the index" in proc_run_info.stderr:
            return
        print("ERROR:")
        print(proc_run_info.stdout)
        print(proc_run_info.stderr)
        raise NonZeroExitCodeError(
            "For '%s' Exit code was %d" % (" ".join(cmd_list), ret_code)
        )
