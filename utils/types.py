from pathlib import Path
from typing import Callable, Optional, Tuple, Union

PostInstallCallBack = Callable[[Path], None]
GitUri = str
GitBranchName = Optional[str]
GitUriWithBranch = Tuple[GitUri, GitBranchName]
GitInfo = Union[GitUri, GitUriWithBranch]
PluginSimpleDef = Tuple[GitInfo, str, str, str]
PluginWithCallbackDef = Tuple[str, str, str, str, PostInstallCallBack]
PluginParsed = Tuple[
    GitUri, GitBranchName, str, str, Path, str, Optional[PostInstallCallBack]
]
PluginDef = Union[PluginSimpleDef, PluginWithCallbackDef]
