from pathlib import Path
from typing import List

from utils.decompose_info import decompose
from utils.types import PluginDef


def print_plug(plugin: PluginDef, pack_path: Path) -> None:
    """Prints a nice version of the plugin for help and discovery."""
    (
        url,
        branch_name,
        pretty_name,
        summary,
        install_location,
        my_mini_doc,
        post_install_callback,
    ) = decompose(
        plugin,
        pack_path,
    )
    print(
        f"{pretty_name}:\n  | {summary}\n  | {install_location}\n  | HELP {my_mini_doc}"
    )


def print_plugin_list(plugins: List[PluginDef], pack_path: Path) -> None:
    """Prints the given list of plugin definitions nicely."""
    for plugin_def in plugins:
        print_plug(plugin_def, pack_path)
