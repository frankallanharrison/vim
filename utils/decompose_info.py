from pathlib import Path
from typing import Optional

from utils.types import (
    GitBranchName,
    GitInfo,
    GitUri,
    PluginDef,
    PluginParsed,
    PostInstallCallBack,
)


def decompose(plugin: PluginDef, pack_path: Path) -> PluginParsed:
    git_info: GitInfo = plugin[0]
    url: GitUri
    branch_name: GitBranchName = None
    if isinstance(git_info, str):
        url = git_info
    else:
        assert (
            len(git_info) == 2
        ), "Unexpected number of elements in git-info, should be (uri, branch-name)"
        url = git_info[0]
        branch_name = git_info[1]

    pretty_name: str = plugin[1]
    install_location: Path = pack_path / pretty_name
    summary: str = plugin[2]
    my_mini_doc: str = plugin[3]
    post_install_callback: Optional[PostInstallCallBack] = None
    try:
        post_install_callback = plugin[4]  # type: ignore # we have a union type here
    except IndexError:
        pass  # skip for now
    return (
        url,
        branch_name,
        pretty_name,
        summary,
        install_location,
        my_mini_doc,
        post_install_callback,
    )
